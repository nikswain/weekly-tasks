//
//  HistoryViewCell.swift
//  Weekly Tasks
//
//  Created by Nik Swain on 2016-06-07.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import UIKit

class HistoryViewCell : UITableViewCell
{
    @IBOutlet weak var dtmLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    func configure(dtm: String, action:String, name: String)
    {
        self.dtmLabel.text = dtm
        self.actionLabel.text = action
        self.nameLabel.text = name
        
        if((action == "Complete") || (action == "Completed")){
            self.actionLabel.textColor = UIColor(red: 0.42, green: 0.66 , blue: 0.31, alpha:1.0)
        } else if(action == "Deleted"){
            self.actionLabel.textColor = UIColor.redColor()
        }
    }

}