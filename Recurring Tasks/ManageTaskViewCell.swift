//
//  ManageTaskViewCell.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-06-01.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import UIKit

class ManageTaskViewCell : UITableViewCell
{
    @IBOutlet weak var TaskTitleLabel: UILabel!
    @IBOutlet weak var Status1Label: UILabel!
    @IBOutlet weak var Status2Label: UILabel!
    
    func configure(title:String, status1:String, status2:String)
    {
        TaskTitleLabel.text = title
        Status1Label.text = status1
        Status2Label.text = status2
        
        if((status1 == "Complete") || (status1 == "Completed")){
            self.Status1Label.textColor = UIColor(red: 0.42, green: 0.66 , blue: 0.31, alpha:1.0)
        }
    }
    
}