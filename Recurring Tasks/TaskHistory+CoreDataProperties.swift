//
//  TaskHistory+CoreDataProperties.swift
//  Weekly Tasks
//
//  Created by Nik Swain on 2016-06-07.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension TaskHistory {

    @NSManaged var taskid: String?
    @NSManaged var taskname: String?
    @NSManaged var dtm: NSDate?
    @NSManaged var action: String?

}
