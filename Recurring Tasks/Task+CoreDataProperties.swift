//
//  Task+CoreDataProperties.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-31.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Task {

    @NSManaged var id: String?
    @NSManaged var name: String?
    @NSManaged var notes: String?
    @NSManaged var period: NSNumber?
    @NSManaged var reset: NSNumber?
    @NSManaged var startdate: NSDate?
    @NSManaged var onetime: NSNumber?

}
