//
//  HistoryMasterViewController.swift
//  Weekly Tasks
//
//  Created by Nik Swain on 2016-06-07.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class HistoryMasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    var managedObjectContext: NSManagedObjectContext? = nil
    var historyDataAccess: HistoryDataAccess? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyDataAccess = HistoryDataAccess(managedObjectContext: self.managedObjectContext!, delegate: self)
    }
    
    override func viewDidAppear(animated: Bool) {
        historyDataAccess!.Reload(managedObjectContext!)
        self.tableView.reloadData()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.historyDataAccess!.fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.historyDataAccess!.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("HistoryMasterCell", forIndexPath: indexPath) as! HistoryViewCell
        let object = self.historyDataAccess!.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
        self.configureCell(cell, withObject: object)
        
        return cell
    }
    
    func configureCell(cell: UITableViewCell, withObject object: NSManagedObject) {
        
        let historyItem = object as! TaskHistory
        let historycell = cell as! HistoryViewCell
        let dtm = historyItem.dtm?.shortFormatWithTime()
        let action = historyItem.action!
        if let taskname = historyItem.taskname {
            historycell.configure(dtm!, action: action, name: taskname)
        }
    }
    
}
    
