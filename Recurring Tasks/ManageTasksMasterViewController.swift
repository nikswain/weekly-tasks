//
//  MasterViewController.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-26.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import UIKit
import CoreData

class ManageTasksMasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var detailViewController: ManageTasksDetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    var taskDataAccess: TaskDataAccess? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(insertNewObject(_:)))
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? ManageTasksDetailViewController
        }
        
        self.taskDataAccess = TaskDataAccess(managedObjectContext: self.managedObjectContext!, delegate: self)
        self.taskDataAccess!.loadAllItems()
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.taskDataAccess!.loadAllItems()
        self.tableView.reloadData()
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func insertNewObject(sender: AnyObject) {
        // Show detail page - with nil detailObject and it will insert for us
        performSegueWithIdentifier("showDetail", sender: nil)
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showDetail") {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = self.taskDataAccess!.fetchedResultsController.objectAtIndexPath(indexPath)
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! ManageTasksDetailViewController
                controller.managedObjectContext = self.managedObjectContext
                controller.detailItem = object as? Task
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            } else {
                // This is a new item because nothing is selected
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! ManageTasksDetailViewController
                controller.managedObjectContext = self.managedObjectContext
                
            }
        }
    }

    
    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.taskDataAccess!.fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.taskDataAccess!.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ManageTaskMasterCell", forIndexPath: indexPath)
        let object = self.taskDataAccess!.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
        self.configureCell(cell, withObject: object)
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let context = self.taskDataAccess!.fetchedResultsController.managedObjectContext
            let object = self.taskDataAccess!.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
            context.deleteObject(object)
            
            // Record it in the history
            // Record the task being completed in the history
            let task = object as! Task
            let newHistoryObject = NSEntityDescription.insertNewObjectForEntityForName("TaskHistory", inManagedObjectContext: self.managedObjectContext!) as! TaskHistory
            newHistoryObject.taskid = task.id
            newHistoryObject.taskname = task.name
            newHistoryObject.action = "Deleted"
            newHistoryObject.dtm = NSDate()
            
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
            }
        }
    }

    func configureCell(cell: UITableViewCell, withObject object: NSManagedObject) {
        let taskCell = cell as! ManageTaskViewCell
        let task = object as! Task
        
        let status1 = task.isActive() ? "Active" : " "
        
        let status2 = String.init(format:"Start: %@ Period: %@", task.startdate!.shortFormat(), task.period!)
        taskCell.configure(task.name!, status1: status1, status2: status2)
    }

    // MARK: - Fetched results controller delegate functions
     // Implementing the above methods (didChangeObject etc) to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
         // In the simplest, most efficient, case, reload the table view.
         self.tableView.reloadData()
     }
 
    
}

