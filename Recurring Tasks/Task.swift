//
//  Task.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-31.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import CoreData


class Task: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func isActive() -> Bool {
        var result = false
        
        let start = self.startdate?.dateOnly()
        let today = NSDate().dateOnly()
        let daysAfterStartDate = today.daysFrom(start!)
        
        if (daysAfterStartDate >= 0){
            result = true
        }
        
        return result
    }
    
    var debugString : String {
        return self.name! + ", " + self.description
    }
    
    var startDateString : String {
        return self.startdate!.shortFormatWithTime()
    }
    
    func dueDate() -> NSDate {
        let dueDate = NSCalendar.currentCalendar().dateByAddingUnit(
                        .Day,
                        value: Int(self.period!),
                        toDate: self.startdate!.dateOnly(),
                        options: NSCalendarOptions(rawValue: 0))
        return dueDate!
    }
    
    func daysLeft() -> Int {
        let daysLeft = self.dueDate().daysFrom(NSDate().dateOnly())
        return daysLeft
    }
    
}
