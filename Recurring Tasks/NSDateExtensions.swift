//
//  NSDateExtensions.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-30.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation

extension NSDate{

    
    func shortFormat() -> String {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = .NoStyle
        
        return formatter.stringFromDate(self)
    }

    func shortFormatWithTime() -> String {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = .ShortStyle
        
        return formatter.stringFromDate(self)
    }

    func dayName() -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEEE"
        let dayName = formatter.stringFromDate(self)
        return dayName
    }
    
    func dateOnly() -> NSDate {
        let components = NSCalendar.currentCalendar().components([.Month,  .Year,  .Day], fromDate: self)
        let dateOnly = NSCalendar.currentCalendar().dateWithEra(1, year: components.year, month: components.month, day: components.day, hour:0, minute:0, second:0, nanosecond: 0)!
        return dateOnly
    }
    
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    
    static func dateYMD(year:Int, month:Int, day:Int) -> NSDate{
        return NSCalendar.currentCalendar().dateWithEra(1, year: year, month: month, day: day, hour:0, minute:0, second:0, nanosecond: 0)!
    }
    
    func addDays(days:Int) -> NSDate{
        return NSCalendar.currentCalendar().dateByAddingUnit(
            .Day,
            value: days,
            toDate: self,
            options: NSCalendarOptions(rawValue: 0))!
    }
    
    // Return todays day number. 1 is Sunday. 7 is Saturday
    func dayOfWeek() -> Int? {
        if
            let cal: NSCalendar = NSCalendar.currentCalendar(),
            let comp: NSDateComponents = cal.components(.Weekday, fromDate: self) {
            return comp.weekday
        } else {
            return nil
        }
    }
    
    // Comparison functions
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
}
