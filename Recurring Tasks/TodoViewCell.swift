//
//  TodoCellViewController.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-27.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import UIKit

class TodoViewCell : UITableViewCell
{
    var id:String = ""
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeStatusLabel: UILabel!
    
    func configure(id:String, title:String, timeStats: String, complete: Bool,  warning: Int)
    {
        self.id = id
        titleLabel.text = title
        timeStatusLabel.text = timeStats
        
        
        if(warning > 0){
            self.titleLabel.textColor = UIColor.redColor()
            self.timeStatusLabel.textColor = UIColor.redColor()
        } else {
            self.titleLabel.textColor = UIColor.blackColor()
            self.timeStatusLabel.textColor = UIColor.blackColor()
        }
    }
    
}