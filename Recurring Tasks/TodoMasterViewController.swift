//
//  TodoMasterViewController.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-27.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TodoMasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    var managedObjectContext: NSManagedObjectContext? = nil
    var taskDataAccess: TaskDataAccess? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        taskDataAccess = TaskDataAccess(managedObjectContext: self.managedObjectContext!, delegate: self)
        taskDataAccess!.loadTodoItems()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        // Make sure we query the DB rather than re-using the last one
        taskDataAccess!.loadTodoItems()
        self.tableView.reloadData()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.taskDataAccess!.fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.taskDataAccess!.fetchedResultsController.sections![section]

        if sectionInfo.numberOfObjects == 0{
            let emptyLabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            emptyLabel.text = "No active tasks. Well done!\r\n\r\nUse Manage Tasks\r\nto add or reactivate tasks"
            emptyLabel.textAlignment = NSTextAlignment.Center
            emptyLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
            emptyLabel.numberOfLines = 4
            self.tableView.backgroundView = emptyLabel
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        } else {
            self.tableView.backgroundView = nil
        }
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let complete = UITableViewRowAction(style: .Normal, title: "Complete") { (action, indexPath) in
            let context = self.taskDataAccess!.fetchedResultsController.managedObjectContext
            
            let task = self.taskDataAccess!.fetchedResultsController.objectAtIndexPath(indexPath) as! Task
            
            // Reset the task for the next time it's needed
            // If period is zero then it's a one off, never needed again
            if(Int(task.period!) > 0) {
                
                // Next start date is last start date plus period
                // until it is after today - if overdue
                var newStartdate = task.startdate!.addDays(Int(task.period!))
                while(newStartdate.isLessThanDate(NSDate().dateOnly()) ||
                      newStartdate.isEqualToDate(NSDate().dateOnly())) {
                    newStartdate = newStartdate.addDays(Int(task.period!))
                }
                task.startdate = newStartdate
            } else {
                context.deleteObject(task)
            }
            
            // Record the task being completed in the history
            let newHistoryObject = NSEntityDescription.insertNewObjectForEntityForName("TaskHistory", inManagedObjectContext: self.managedObjectContext!) as! TaskHistory
            newHistoryObject.taskid = task.id
            newHistoryObject.taskname = task.name
            newHistoryObject.action = "Complete"
            newHistoryObject.dtm = NSDate()
            
            
            // Save everything
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
            
        }
        
        complete.backgroundColor = UIColor(red: 0.42, green: 0.66 , blue: 0.31, alpha:1.0)
        
        return [complete]
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TodoMasterCell", forIndexPath: indexPath) as! TodoViewCell
        
        let object = self.taskDataAccess!.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
        self.configureCell(cell, withObject: object)

        return cell
    }
    
    
    func configureCell(cell: UITableViewCell, withObject object: NSManagedObject) {

        let task = object as! Task

        let daysLeft = task.daysLeft()

        var message = String.init(format:"%d days left", daysLeft)
        if(daysLeft <= 0) {
            message = "Overdue"
        }
        var warning = 0
        if(daysLeft <= 0){
            warning = 1
        }
        let todocell = cell as! TodoViewCell
        todocell.configure(task.id!, title: task.name!, timeStats: message, complete: false,  warning: warning)
    }
    
    
    
    // MARK : Table update delegates
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        // In the simplest, most efficient, case, reload the table view.
        self.tableView.reloadData()
    }
    
}