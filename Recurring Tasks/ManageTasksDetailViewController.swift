//
//  DetailViewController.swift
//  Recurring Tasks
//
//  Created by Nik Swain on 2016-05-26.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import UIKit
import CoreData

class ManageTasksDetailViewController: UIViewController , NSFetchedResultsControllerDelegate{
    var managedObjectContext: NSManagedObjectContext? = nil
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var periodTextField: UITextField!
    @IBOutlet weak var periodStepper: UIStepper!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var startDateDayLabel: UILabel!
    
    var detailItem: Task? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
    
        if let task = self.detailItem {
            if nameTextField != nil {
                nameTextField.text = task.name
                periodTextField.text = String(task.period!)
                periodStepper.value = Double(task.period!)
                startDatePicker.date = task.startdate!
                startDateDayLabel.text = startDatePicker.date.dayName()
            }
        } else {
            // New item
            nameTextField.text = ""
            periodTextField.text = String(0)
            periodStepper.value = 0
            startDatePicker.date = self.lastMonday()
            startDateDayLabel.text = startDatePicker.date.dayName()
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // Do any additional setup after loading the view, typically from a nib.
        self.nameTextField.becomeFirstResponder()
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        if(self.detailItem != nil) {
            // Save the existing task
            let objectToUpdate = self.detailItem! as Task
            objectToUpdate.name = nameTextField.text
            objectToUpdate.period = Int(periodTextField.text!)
            objectToUpdate.startdate = startDatePicker.date.dateOnly()
            
            do {
                try self.managedObjectContext!.save()
            } catch {
                // Do something in response to error condition
            }
        } else {
            // Add a new item if a name was entered
            if(nameTextField.text != ""){
                let newManagedObject = NSEntityDescription.insertNewObjectForEntityForName("Task", inManagedObjectContext: self.managedObjectContext!) as! Task
                
                newManagedObject.id = NSUUID().UUIDString
                newManagedObject.name = nameTextField.text
                let period = Int(periodTextField.text!)
                newManagedObject.period = period
                newManagedObject.startdate = startDatePicker.date.dateOnly()

                do {
                    try self.managedObjectContext!.save()
                    
                } catch {
                    abort()
                }
            }
        }  
    }

    // MARK: - UI Events
    
    @IBAction func startDateChanged(sender: AnyObject) {
        startDateDayLabel.text = sender.date.dayName()
        view.endEditing(true)
    }

    @IBAction func periodStepperChange(sender: UIStepper) {
        periodTextField.text = String(Int(sender.value))
        view.endEditing(true)
    }
    

    
    // MARK: Helper functions
    func lastMonday() -> NSDate {
        let dayOfWeek:Int = (NSDate().dayOfWeek())!
        var daysSinceMonday = (dayOfWeek - 2)
        if(daysSinceMonday == -1)
        {
            daysSinceMonday = 6
        }
        return NSDate().addDays(daysSinceMonday * -1)
    }
}

