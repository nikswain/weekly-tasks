//
//  HistoryDataAccess.swift
//  Weekly Tasks
//
//  Created by Nik Swain on 2016-06-07.
//  Copyright © 2016 Cornerhouse Software. All rights reserved.
//

import Foundation
import Foundation
import CoreData

public class HistoryDataAccess {
    
    private var _fetchedResultsController: NSFetchedResultsController? = nil
    var managedObjectContext:NSManagedObjectContext? = nil
    var delegate: NSFetchedResultsControllerDelegate? = nil
    
    init(managedObjectContext:NSManagedObjectContext, delegate: NSFetchedResultsControllerDelegate){
        self.managedObjectContext = managedObjectContext
        self.delegate = delegate
    }
    
    func Reload(managedObjectContext: NSManagedObjectContext){
        _fetchedResultsController = nil
    }
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("TaskHistory", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate
        let sortDescriptor = NSSortDescriptor(key: "dtm", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = delegate
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
}